import { TranslateValueModel } from '@app/models/translate-value.model';
import { ColorPriceModel } from '@app/models/color-price.model';

export interface TicketsGroupWithOutSchemaModel {
    id: number;
    name: TranslateValueModel;
    available: boolean;
    canBuy: boolean;
    order: number;
    comment: TranslateValueModel;
    colorPrice: ColorPriceModel;
}
