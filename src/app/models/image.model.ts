export interface ImageModel {
  id: number;
  lastModified: string;
  title: string;
  path: string;
  type: string;
  alt: string;
}
