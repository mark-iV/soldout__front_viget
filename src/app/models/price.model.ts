export interface PriceModel {
  bookedCommission: number;
  bookedCommissionPrice: number;
  commission: number;
  price: number;
  priceWithOutCommission: number;
}
