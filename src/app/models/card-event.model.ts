import { TranslateValueModel } from '@app/models/translate-value.model';
import { ImageModel } from '@app/models/image.model';
import { SameEventModel } from '@app/models/same-events.model';
import { PlatformHallModel } from '@app/models/platform-hall.model';

export interface CardEventModel {
  id: number;
  like: boolean;
  platformHall: PlatformHallModel;
  maxPrice: number;
  minPrice: number;
  name: TranslateValueModel;
  dataBegin: string;
  beginSell: string;
  mainImage: ImageModel;
  mainImageSmall: ImageModel;
  sameEvent: SameEventModel[];
  urlSegment: string;
}
