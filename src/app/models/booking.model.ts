import { CommandBooking } from '@app/enums/command-booking.enum';
import { TypeOfScheme } from '@app/enums/type-of-scheme.enum';

export interface BookingModel {
  idSeat?: number;
  idTicket: number;
  idEvent: number;
  order: string;
  comandBooking: CommandBooking;
  typeOfScheme: TypeOfScheme;
}
