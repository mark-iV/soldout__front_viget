import { TranslateValueModel } from '@app/models/translate-value.model';
import { ImageModel } from '@app/models/image.model';
import { SameEventModel } from '@app/models/same-events.model';
import { VideoModel } from '@app/models/video.model';
import { EventCategoryModel } from '@app/models/event-category.model';
import { CardEventModel } from '@app/models/card-event.model';
import { PlatformHallModel } from '@app/models/platform-hall.model';

export interface EventModel {
  id: number;
  like: boolean;
  platformHall: PlatformHallModel;
  maxPrice: number;
  minPrice: number;
  name: TranslateValueModel;
  dataBegin: any;
  beginSell: string;
  mainImage: ImageModel;
  mainImageSmall: ImageModel;
  sameEvent: SameEventModel[];
  urlSegment: string;
  ticketOrderTimeBoxOffice: number;
  ticketOrderTime: number;
  shortDescription: TranslateValueModel;
  description: TranslateValueModel;
  image: ImageModel[];
  videos: VideoModel[];
  similar: CardEventModel[];
  eventCategory: EventCategoryModel[];
  beginEndDate?: string;
  eventGenre?: any[];
}
