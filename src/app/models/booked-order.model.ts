import { ImageModel } from '@app/models/image.model';
import { PlatformHallModel } from '@app/models/platform-hall.model';
import { TranslateValueModel } from '@app/models/translate-value.model';
import { SeatModel } from '@app/models/seat.model';
import { ColorPriceModel } from '@app/models/color-price.model';

interface Event {
  id: number;
  lastModified: string;
  mainImage: ImageModel;
  mainImageSmall: ImageModel;
  name: TranslateValueModel;
  beginDate: string;
  urlSegment: string;
  beginEndDate: string;
  platformHall: PlatformHallModel;
  maxPrice: number;
  minPrice: number;
}

interface TicketList {
  id: number;
  sector: string;
  row: string;
  seatPosition: string;
  seat: SeatModel;
  colorPrice: ColorPriceModel;
  ticketGroupWithOutSachem: boolean;
  barcode: string;
  commissionBooked: number;
}

export interface BookedOrderModel {
  id: number;
  lastModified: string;
  created: string;
  timeUnlock: string;
  binStatus: 'BOOKED';
  ticketList: TicketList[];
  event: Event;
  price: number;
  order: string;
  comment: string;
  bookedWithPay: boolean;
  timeLockInTime: number;
}
