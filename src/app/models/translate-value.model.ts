export interface TranslateValueModel {
  id: number;
  lastModified: string;
  valueEN: string;
  valueRU: string;
  valueUA: string;
}
