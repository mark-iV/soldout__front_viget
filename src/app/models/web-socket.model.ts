export interface WebSocketModel {
  seatId: number;
  eventId: number;
  colorPrice: number;
  ticketId: number;
  seatKey: string;
  fanzone: boolean;
  order: string;
}
