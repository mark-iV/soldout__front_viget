import { TranslateValueModel } from '@app/models/translate-value.model';

export interface EventCategoryModel {
  id: number;
  lastModified: string;
  name: TranslateValueModel;
}
