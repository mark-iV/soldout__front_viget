import { TranslateValueModel } from '@app/models/translate-value.model';

export interface HallModel {
  id: number;
  lastModified: string;
  name: TranslateValueModel;
  shortName: TranslateValueModel;
}
