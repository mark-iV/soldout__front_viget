import { ImageModel } from '@app/models/image.model';
import { TranslateValueModel } from '@app/models/translate-value.model';

export interface SameEventModel {
    id: number;
    lastModified: string;
    mainImage: ImageModel;
    mainImageSmall: ImageModel;
    name: TranslateValueModel;
    beginDate: string;
    urlSegment: string;
}
