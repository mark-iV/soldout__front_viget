import { TranslateValueModel } from '@app/models/translate-value.model';

export interface PlatformHallModel {
  id: number;
  lastModified: string;
  address: TranslateValueModel;
  city: {
    id: number;
    lastModified: string;
    dictionary: TranslateValueModel;
    region: {
      id: number,
      lastModified: string,
      dictionary: TranslateValueModel;
      country: {
        id: number,
        lastModified: string;
        dictionary: TranslateValueModel;
        available: boolean;
        geoCode: string;
      },
      available: boolean;
      geoCode: string;
    },
    available: boolean;
    postalCode: string;
  };
  name: TranslateValueModel;
}
