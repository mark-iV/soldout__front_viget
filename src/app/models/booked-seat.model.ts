import { SeatModel } from '@app/models/seat.model';
import { ColorPriceModel } from '@app/models/color-price.model';
import { TicketsGroupWithOutSchemaModel } from '@app/models/tickets-group-with-out-schema.model';

export interface BookedSeatModel {
  id: number;
  sector: string;
  row: string;
  seatPosition: string;
  seat: SeatModel;
  colorPrice: ColorPriceModel;
  ticketGroupWithOutSachem: TicketsGroupWithOutSchemaModel;
  barcode: string;
}
