export interface ColorPriceModel {
  id: number;
  lastModified: string;
  color: string;
  price: number;
  bonusGet: number;
  bonusSell: number;
  available: boolean;
}
