import { TranslateValueModel } from '@app/models/translate-value.model';

export interface VenueResponseFull {
  content: [];
  first: boolean;
  last: boolean;
  number: number;
  numberOfElements: number;
  size: number;
  sort: any;
  totalElements: number;
  totalPages: number;
}

export interface VenueName {
  dictionary: TranslateValueModel;
    id: number;
    lastModified: string;
    valueEN: string;
    valueRU: string;
    valueUA: string;
}

export interface VenueContent {
  beginSell: string;
  dataBegin: string;
  id: number;
  like: boolean;
  urlSegment: string;
  maxPrice: number;
  minPrice: number;
  name: VenueName;
}

export interface VenuePlatformHall {
  imgUrl: string;
  like: boolean;
  name: VenueName;
  platformHallId: number;
  address: VenueName;
  amount: number;
  city: VenueName;
  dataBegin: string;
}

export interface VenueState {
  loading: boolean;
  loaded: boolean;
  content: VenuePlatformHall[];
}

export interface VenueResponseMapped {
  content: VenuePlatformHall[];
}
