export interface SeatModel {
  id: number;
  key: string;
  fanzone: boolean;
  count: number;
}
