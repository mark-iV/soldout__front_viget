import { ImageModel } from '@app/models/image.model';

export interface VideoModel {
  id: number;
  lastModified: string;
  path: string;
  poster: ImageModel;
  type: string;
  videoType: string;
}
