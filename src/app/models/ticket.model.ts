import { ColorPriceModel } from '@app/models/color-price.model';
import { SeatModel } from '@app/models/seat.model';

export interface TicketModel {
  id: number;
  seat: SeatModel;
  colorPrice: ColorPriceModel;
  ticketGroupWithOutSachem: any;
}
