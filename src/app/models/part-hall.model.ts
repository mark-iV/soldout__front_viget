export interface PartHallModel {
  id: number;
  lastModified: string;
  type: string;
  routingName: string;
  pattern: string;
}
