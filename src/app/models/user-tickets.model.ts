import { VenueName, VenuePlatformHall } from '@app/models/venue.model';

export interface ActualUserTicketsResponse {
  content: any[];
  first: boolean;
  last: boolean;
  number: number;
  numberOfElements: number;
  size: number;
  sort?: any;
  totalElements: number;
  totalPages: number;
}

export interface TicketsList {
  barcode: string;
  colorPrice: any;
  id: number;
  row: string;
  seat: any;
  seatPosition: string;
  sector: string;
  ticketGroupWithOutSachem; any;
  usedBonuses?: number;
}

export interface UserTicket {
  img: string;
  imgSmall?: string;
  ticketName: VenueName;
  platformHall: VenuePlatformHall;
  beginDate: string;
  beginEndDate: string;
  id: number;
  orderID: number;
  dateOfBuying: string;
  ticketsAmount: number;
  price: number;
  finalPrice: number;
  nominalPrice: number;
  commissionBookedInMoney: number;
  ticketsList: TicketsList[];
  promo?: number;
  usedBonuses?: number;
  userShortDto?: any;
  timeUnlock?: string;
  isBooked?: boolean;
  finalPriceWithBookedCommission?: number;
  externalCommision?: number;
  commissionForBooking?: number;
  // timeUnlock: string;
}

export interface UserTicketState {
  tickets: UserTicket[];
}
