import {Routes} from '@angular/router';

export const appRoutes: Routes = [
  {path: '', loadChildren: 'app/booking/booking.module#BookingModule'},
];

