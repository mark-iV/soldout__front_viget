import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { API_URL } from '../../environments/environment';
import { HallModel } from '@app/models/hall.model';
import { ColorPriceModel } from '@app/models/color-price.model';
import { TicketsGroupWithOutSchemaModel } from '@app/models/tickets-group-with-out-schema.model';

@Injectable({
  providedIn: 'root',
})
export class HallService {

  constructor(private httpClient: HttpClient) { }

  getHall(eventId: number): Observable<HallModel> {
    return this.httpClient.get<HallModel>(`${API_URL}hall/client/find-one-by-event/${eventId}`);
  }

  getPartHall(hallId: number): Observable<any> {
    return this.httpClient.get<any>(`${API_URL}hall/client/find-first-part-hall/${hallId}`);
  }

  getFreeTickets(eventId: number, partHallId: number): Observable<any> {
    return this.httpClient.get<any>(`${API_URL}event/client/online/find-all-free-ticket-by-event-id-and-part-hall-id/${eventId}/${partHallId}`);
  }

  getFreeTicketsWithOutSchema(eventId: number): Observable<any> {
    return this.httpClient.get<any>(`${API_URL}event/client/online/find-all-free-ticket-by-event-id-with-out-schema/${eventId}`);
  }

  getAllTicketsGroupWithOutSchema(eventId: number): Observable<TicketsGroupWithOutSchemaModel[]> {
    return this.httpClient.get<TicketsGroupWithOutSchemaModel[]>(`${API_URL}event/client/online/find-all-ticket-group-with-out-schema/${eventId}`);
  }

  getFree(eventId: number): Observable<any> {
    return this.httpClient.get<any>(`${API_URL}event/client/online/find-all-free-tickets-by-event-id/${eventId}`);
  }

  getPriceColors(eventId: number, partHallId: number): Observable<ColorPriceModel[]> {
    return this.httpClient.get<ColorPriceModel[]>(`${API_URL}color-price/find-with-schema-by-part-hall-id-and-event-id/${eventId}/${partHallId}`);
  }

}
