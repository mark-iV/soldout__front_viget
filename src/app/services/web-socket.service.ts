import { Injectable } from '@angular/core';
import { WebSocketModel } from '@app/models/web-socket.model';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { Subject } from 'rxjs';
import { WS_URL } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class WebSocketService {
  private stompClient: Stomp;

  private _lockWebSocketData = new Subject<WebSocketModel>();
  lockWebSocketData$ = this._lockWebSocketData.asObservable();

  private _unLockWebSocketData = new Subject<WebSocketModel>();
  unLockWebSocketData$ = this._unLockWebSocketData.asObservable();


  initializeWebSocketConnection(id: number) {
    const ws = new SockJS(WS_URL);
    this.stompClient = Stomp.over(ws);
    this.stompClient.debug = () => {};
    this.stompClient.connect({}, (frame) => {
      // this.stompClient.subscribe('/service.locker.' + id, (message) => {
      //   this._lockWebSocketData.next(JSON.parse(message.body));
      // }, (error) => {
      //   // console.error(error);
      // });
      // this.stompClient.subscribe('/service.un-locker.' + id, (message) => {
      //   this._unLockWebSocketData.next(JSON.parse(message.body));
      // }, (error) => {
      //   // console.error(error);
      // });
      this.stompClient.subscribe('/topic/locker.' + id, (message) => {
        this._lockWebSocketData.next(JSON.parse(message.body));
      }, (error) => {
        // console.error(error);
      });
      this.stompClient.subscribe('/topic/un-locker.' + id, (message) => {
        this._unLockWebSocketData.next(JSON.parse(message.body));
      }, (error) => {
        // console.error(error);
      });
    });
  }

  disconnect() {
    if (this.stompClient) {
      try {
        this._lockWebSocketData = new Subject<WebSocketModel>();
        this.lockWebSocketData$ = this._lockWebSocketData.asObservable();
        this._unLockWebSocketData = new Subject<WebSocketModel>();
        this.unLockWebSocketData$ = this._unLockWebSocketData.asObservable();
        this.stompClient.disconnect();
        // this.idEvent.forEach(value => {
        //   this.stompClient.unsubscribe('/service.locker.' + value);
        //   this.stompClient.unsubscribe('/service.un-locker.' + value);
        // });
        // this.idEvent = [];
      } catch (e) {
        // console.error(e);
      }
    }
  }
}
