import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_URL, EndPoints } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EventService {

  constructor(private httpClient: HttpClient) { }

  getEvent(id: number): Observable<any> {
    return this.httpClient.get<any>(`${API_URL}${EndPoints.getEvent}/${id}`);
  }

}
