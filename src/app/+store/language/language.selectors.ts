import { createFeatureSelector } from '@ngrx/store';
import { LanguageModel } from '@app/models/language.model';

export const getLanguageState = createFeatureSelector<LanguageModel>('language');
