import { createFeatureSelector, createSelector } from '@ngrx/store';
import { BookingState } from '@app/+store/booking/booking.state';


export const getBookingState = createFeatureSelector<BookingState>('booking');

export const getBin = createSelector(getBookingState, (state: BookingState) => state.bin);
export const getPrice = createSelector(getBookingState, (state: BookingState) => state.price);

