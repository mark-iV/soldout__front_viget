import {  AfterViewChecked, AfterViewInit, ChangeDetectorRef,  Component,  ElementRef,  EventEmitter,
  Inject,  Input,  OnChanges,  OnDestroy,  OnInit,  Output,  PLATFORM_ID,  SimpleChanges,  ViewChild,
} from "@angular/core";
import {isNull, isNullOrUndefined} from "util";
import {HallService} from "@app/services/hall.service";
import {HallModel} from "@app/models/hall.model";
import {PartHallModel} from "@app/models/part-hall.model";
import {TicketModel} from "@app/models/ticket.model";
import {ColorPriceModel} from "@app/models/color-price.model";
import {BookingService} from "@app/services/booking.service";
import {CommandBooking} from "@app/enums/command-booking.enum";
import {WebSocketService} from "@app/services/web-socket.service";
import {BookedSeatModel} from "@app/models/booked-seat.model";
import {SeatColorsEnum} from "@app/app.settings";
import {IAppState} from "@app/+store";
import {select, Store} from "@ngrx/store";
import {Booking, GetBin} from "@app/+store/booking/booking.actions";
import {CommonService} from "@app/services";
import {TypeOfScheme} from "@app/enums/type-of-scheme.enum";
import {SnackBarService} from "@app/services/snack-bar.service";
import {TicketsGroupWithOutSchemaModel} from "@app/models/tickets-group-with-out-schema.model";
import {Router} from "@angular/router";
import {isPlatformBrowser} from "@angular/common";
import {TranslateService} from "@ngx-translate/core";
import {combineLatest, Observable, Subject} from "rxjs";
import {mergeMap, take, takeUntil} from "rxjs/operators";
import {WebSocketModel} from "@app/models/web-socket.model";
import {getBin} from "@app/+store/booking/booking.selector";

@Component({
  selector: "app-booking-hall-map",
  templateUrl: "./booking-hall-map.component.html",
  styleUrls: ["./booking-hall-map.component.scss"],
})
export class BookingHallMapComponent
  implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  @ViewChild("hallMap") hallMapRef: ElementRef;
  hallMapEl: HTMLDivElement;

  @ViewChild("hallMapWrapper") hallMapWrapperRef: ElementRef;
  hallMapWrapperEl: HTMLDivElement;

  @Input() bin: BookedSeatModel[];
  @Input() eventId: number;
  @Input() event: any;
  @Input() lang = "ua";
  @Input() isSelectedSeat: Subject<boolean>;

  @Output() removedTicket: EventEmitter<number> = new EventEmitter<number>();
  @Output() emitColorPrices: EventEmitter<any> = new EventEmitter<any>();

  seatInfo;
  tooltipSeatStyles;
  colorPrices: ColorPriceModel[] = [];
  zoomIsActive = false;
  stop = false;
  zooms = 1;
  pinchValue = 1;
  partHall: any;
  tickets = [];
  ticketsGroupWithOutSchema: TicketsGroupWithOutSchemaModel[] = [];
  ticketsDto = [];
  orderId: string;
  tempX: number;
  tempY: number;
  bookingTickets = {} as any;
  subSolidLock;
  subSolidUnlock;
  subWithoutLock;
  subWithoutUnlock;
  hall: HallModel;
  seats: any;
  seats2: any;
  SeatColorsEnum: typeof SeatColorsEnum = SeatColorsEnum;
  filterPriceState: any[] = [];
  isMobileDevice: Boolean;
  selectedTickets: BookedSeatModel[];
  allTickets: TicketModel[];
  typeOfScheme: TypeOfScheme;
  destroy: Subject<any> = new Subject<any>();

  constructor(
    private hallService: HallService,
    private bookingService: BookingService,
    private webSocketService: WebSocketService,
    private store: Store<IAppState>,
    private commonService: CommonService,
    private snackBarService: SnackBarService,
    private router: Router,
    private translateService: TranslateService,
    @Inject(PLATFORM_ID) private platformId: Object) {
  }

  detectmob() {
    if (
      navigator.userAgent.match(/Android/i) ||
      navigator.userAgent.match(/webOS/i) ||
      navigator.userAgent.match(/iPhone/i) ||
      navigator.userAgent.match(/iPad/i) ||
      navigator.userAgent.match(/iPod/i) ||
      navigator.userAgent.match(/BlackBerry/i) ||
      navigator.userAgent.match(/Windows Phone/i)
    ) {
      this.isMobileDevice = true;
    } else {
      this.isMobileDevice = false;
    }
  }

  ngOnInit(): void {
    this.detectmob();
    //
    // this.notifyService.notifyObservable$.pipe()
    //   .subscribe((res) => {
    //     if (res.hasOwnProperty('option') && (res.option === 'updateHull' )) {
    //       console.log('updateHull');
    //       this.loadData();
    //     }
    //   });
    this.hallMapEl = this.hallMapRef.nativeElement;
    this.hallMapWrapperEl = this.hallMapWrapperRef.nativeElement;
    /* TODO: move all to redux if u have more time than I had had */
    // // dispatchers
    // this.store.dispatch(HallActions.getHall(this.eventId));
    //
    // // subscribes
    // this.store.pipe(select(getColorPrice)).subscribe((colors: ColorPriceModel[]) => {
    //   this.colorPrices = colors;
    // });
    //
    //
    // this.store.pipe(select(getCurrentHall)).subscribe((hall: HallModel) => {
    //   this.hall = hall;
    //
    //   if (isNullOrUndefined(this.hall)) {
    //     // this.store.dispatch(HallActions.getFreeTicketsWithOutSchema(this.eventId));
    //     // this.store.dispatch(HallActions.getAllTicketsGroupWithOutSchema(this.eventId));
    //     // // getPartHall
    //     // // getFreeTickets
    //     // this.store.pipe(
    //     //   select(getFreeTicketsWithOutSchema),
    //     //   switchMap((tickets) => {
    //     //     this.ticketsDto = tickets;
    //     //     return this.store.pipe(select(getAllTicketsGroupWithOutSchema));
    //     //   }),
    //     // ).subscribe((next) => {
    //     //   if (!isNullOrUndefined(this.commonService.getOrderFromLocalStorage(this.eventId))) {
    //     //     this.orderId = this.commonService.getOrderFromLocalStorage(this.eventId);
    //     //     this.store.dispatch(new GetBin(this.orderId));
    //     //   }
    //     //
    //     //   this.subWithoutLock = this.webSocketService.lockWebSocketData$.subscribe(({ ticketId }: WebSocketModel) => {
    //     //     this.ticketsDto.splice(this.getIndexOfArray(this.ticketsDto, ticketId), 1);
    //     //   }, error => console.error(error));
    //     //
    //     //   this.subWithoutUnlock = this.webSocketService.unLockWebSocketData$.subscribe(({ ticketId, colorPrice, seatId }: WebSocketModel) => {
    //     //     if (isNull(this.searchTicketById(ticketId, this.ticketsDto))) {
    //     //       const ticket = {
    //     //         id: ticketId,
    //     //         colorPrice: { id: colorPrice },
    //     //         ticketGroupWithOutSachem: { id: seatId },
    //     //       };
    //     //       this.ticketsDto.push(ticket);
    //     //     }
    //     //     const seatKey = this.searchTicketById(ticketId, this.tickets).seat.key;
    //     //     document.getElementById(seatKey).style.fill = this.getColorPriceById(colorPrice).color;
    //     //   }, error => console.error(error));
    //     //
    //     // });
    //   } else {
    //     this.store.dispatch(HallActions.getPartHall(this.hall.id));
    //
    //     this.store.pipe(select(getPartHall)).subscribe((partHall: PartHallModel) => {
    //       if (isNull(partHall)) { return;}
    //       this.partHall = partHall;
    //
    //       if (this.partHall.type === PartHallTypes.SOLID_PART) {
    //         this.store.dispatch(HallActions.getFreeTickets(this.eventId, this.partHall.id));
    //         this.store.dispatch(HallActions.getPriceColors(this.eventId, partHall.id));
    //
    //
    //         this.store.pipe(select(getFreeTickets)).subscribe((tickets: TicketModel[]) => {
    //           this.tickets = tickets;
    //           this.paintSeats(tickets);
    //
    //           if (!isNullOrUndefined(this.commonService.getOrderFromLocalStorage(this.eventId))) {
    //             this.orderId = this.commonService.getOrderFromLocalStorage(this.eventId);
    //             this.store.dispatch(new GetBin(this.orderId));
    //           }
    //
    //           this.subSolidLock = this.webSocketService.lockWebSocketData$
    //             .subscribe(({ ticketId }: WebSocketModel) => {
    //               const seat = this.searchTicketById(ticketId, this.tickets).seat;
    //               if (seat || (seat.fanzone === false)) {
    //                 document.getElementById(seat.key).style.fill = SeatColorsEnum.Booked;
    //               }
    //               this.tickets.splice(this.getIndexOfArray(this.tickets, ticketId), 1);
    //             }, error => console.error(error));
    //
    //           this.subSolidUnlock = this.webSocketService.unLockWebSocketData$
    //             .subscribe(({ ticketId, colorPrice, eventId, seatId, seatKey, fanzone }: WebSocketModel) => {
    //               const ticket = {
    //                 id: ticketId,
    //                 colorPrice: { id: colorPrice },
    //                 event: { id: eventId },
    //                 seat: {
    //                   id: seatId,
    //                   key: seatKey,
    //                   fanzone,
    //                 },
    //               };
    //               this.tickets.push(ticket);
    //               document.getElementById(seatKey).style.fill = this.getColorPriceById(colorPrice).color;
    //             }, error => console.error(error));
    //         });
    //
    //
    //
    //
    //       } else if (this.partHall.type === PartHallTypes.MAIN_PART) {
    //         this.psevdoAfterViewInit();
    //       }
    //
    //
    //     });
    //   }
    // });
    //
    // const orderId = this.commonService.getOrderFromLocalStorage(this.eventId);
    //
    // if (orderId) {
    //   this.store.dispatch(new GetPrice(orderId));
    // }
    //
    // this.webSocketService.initializeWebSocketConnection(this.eventId);
    if (isPlatformBrowser(this.platformId)) {
      this.loadData();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes["eventId"]) {
      // this.loadData();
    }
  }

  ngOnDestroy() {
    this.webSocketService.disconnect();
    this.destroy.next(null);
    this.destroy.complete();
  }

  loadData() {
    if (isPlatformBrowser(this.platformId)) {
      this.seatInfo = null;
      this.tooltipSeatStyles = null;
      this.colorPrices = [];
      this.zoomIsActive = false;
      this.stop = false;
      this.zooms = 1;
      // this.partHall = { pattern: 'Loading...' };
      this.tickets = [];
      this.ticketsGroupWithOutSchema = [];
      this.ticketsDto = null;
      this.orderId = null;
      this.tempX = null;
      this.tempY = null;
      this.bookingTickets = {} as any;
      // if (this.subSolidLock && this.subSolidLock.unsubscribe) {
      //   this.subSolidLock.unsubscribe();
      //   this.subSolidLock = null;
      // }
      // if (this.subSolidUnlock && this.subSolidUnlock.unsubscribe) {
      //   this.subSolidUnlock.unsubscribe();
      //   this.subSolidUnlock = null;
      // }
      // if (this.subWithoutLock && this.subWithoutLock.unsubscribe) {
      //   this.subWithoutLock.unsubscribe();
      //   this.subWithoutLock = null;
      // }
      // if (this.subWithoutUnlock && this.subWithoutUnlock.unsubscribe) {
      //   this.subWithoutUnlock.unsubscribe();
      //   this.subWithoutUnlock = null;
      // }
      this.hall = null;
      this.webSocketService.disconnect();
      this.webSocketService.initializeWebSocketConnection(this.eventId);
      this.typeOfScheme = this.event.typeOfScheme;
      const allTickets$: Observable<TicketModel[]> = this.hallService
        .getFree(this.eventId)
        .pipe(take(1));
      const partHall$: Observable<PartHallModel> = this.hallService
        .getHall(this.eventId)
        .pipe(
          take(1),
          mergeMap(data => this.hallService.getPartHall(data.id).pipe(take(1))));
      this.orderId = this.commonService.getOrderFromLocalStorage(this.eventId);
      if (this.typeOfScheme === TypeOfScheme.WITHOUT_SCHEME) {
        allTickets$.subscribe(tickets => {
          this.allTickets = tickets;
          this.hallService
            .getAllTicketsGroupWithOutSchema(this.eventId)
            .subscribe(ticketsGroupWithOutSchema => {
              this.ticketsGroupWithOutSchema = ticketsGroupWithOutSchema.filter(ticket => ticket.canBuy);
              this.emitColorPrices.emit(this.ticketsGroupWithOutSchema.map(v => v.colorPrice));
              this.fetchBin();
              this.subscritionLock();
              this.subscriptionUnlock();
            });
        });
      } else {
        combineLatest(allTickets$, partHall$).subscribe(
          ([tickets, partHalls]) => {
            this.partHall = partHalls;
            this.allTickets = tickets;
            if (!this.allTickets.length) {
              this.translateService.get("noTicketsForSale").subscribe(text => {
                this.snackBarService.showSnackBar(text);
              });
              // this.snackBarService.showSnackBar('Нажаль всі квитки вже продано!');
              this.router
                .navigate(["/event/" + this.eventId], {queryParams: {lang: this.lang}})
                .then();
            } else {
              if (this.orderId) {
                this.fetchBin();
              } else {
                this.getFreeEventsIfLocalStoreOrNot();
              }
              this.hallService
                .getPriceColors(this.eventId, this.partHall.id)
                .subscribe((colorPrices: ColorPriceModel[]) => {
                  this.colorPrices = colorPrices.sort((a, b) => a.price - b.price);
                  this.emitColorPrices.emit(this.colorPrices);
                });
            }
          });
      }
    }
  }

  public fetchBin() {
    this.store.dispatch(new GetBin(this.orderId));
    if (this.typeOfScheme === TypeOfScheme.WITH_SCHEME) {
      this.store.pipe(take(1), select(getBin)).subscribe(selectedTickets => {
        this.selectedTickets = selectedTickets;
      });
      this.getFreeEventsIfLocalStoreOrNot(this.selectedTickets);
    }
  }

  public getFreeEventsIfLocalStoreOrNot(items?: any) {
    if (isPlatformBrowser(this.platformId)) {
      setTimeout(() => {
        this.paintSeats(this.allTickets);
        if (items) {
          this.paintSeats(items);
          items.forEach(ticket => {
            if (ticket.seat) {
              const el = document.getElementById(ticket.seat.key);
              if (el) {
                if (el.getAttribute("fanzone") === "false") {
                  el.style.fill = SeatColorsEnum.Booked;
                }
              }
            }
          });
        } else {
          // ToDo
        }
      });
      this.subscritionLock();
      this.subscriptionUnlock();
    }
  }

  subscritionLock() {
    this.webSocketService.lockWebSocketData$
      .pipe(takeUntil(this.destroy))
      .subscribe((res: WebSocketModel) => {
        try {
          const ticket = this.allTickets.find(data => data.id === res.ticketId);
          if (this.typeOfScheme === "WITH_SCHEME") {
            if (ticket) {
              if (!ticket.seat.fanzone) {
                if (!this.bin.find(t => t.id === res.ticketId)) {
                  document.getElementById(ticket.seat.key).style.fill =
                    SeatColorsEnum.Disabled;
                }
              }
            }
          }
          this.allTickets.splice(this.getIndexOfArray(this.allTickets, res.ticketId), 1);
        } catch (err) {
        }
      });
  }

  subscriptionUnlock() {
    this.webSocketService.unLockWebSocketData$
      .pipe(takeUntil(this.destroy))
      .subscribe((res: WebSocketModel) => {
        try {
          let ticket: any = {};
          ticket.id = res.ticketId;
          ticket.colorPrice = {id: res.colorPrice};
          if (this.typeOfScheme === "WITH_SCHEME") {
            ticket.event = {id: res.eventId};
            ticket.seat = {
              id: res.seatId,
              key: res.seatKey,
              fanzone: res.fanzone
            };
            let seatKey = document.getElementById(res.seatKey);
            seatKey.style.fill = this.getColorPriceById(res.colorPrice).color;
            seatKey.setAttribute("price", this.getColorPriceById(res.colorPrice).price + "");
            seatKey.setAttribute("seatId", ticket.seat.id + "");
            seatKey.setAttribute("colorPriceId", ticket.colorPrice.id + "");
            seatKey.setAttribute("ticketId", ticket.id + "");
            // seatKey.setAttribute("fanzone", "false");
          } else {
            ticket.ticketGroupWithOutSachem = {id: res.seatId};
          }
          if (isNull(this.searchTicketById(res.ticketId, this.allTickets))) {
            this.allTickets.push(Object.assign({}, ticket));
          }
        } catch (err) {
        }
      });
  }

  psevdoAfterViewInit() {
    if (isPlatformBrowser(this.platformId)) {
      setTimeout(() => {
        if (this.partHall) {
          this.hallMapEl.addEventListener("click", e => {
            if ((<HTMLElement>e.target).classList.contains("section_name")) {
              this.addEventListenerToMainPart(e);
            }
          });
        } else {
          this.psevdoAfterViewInit();
        }
      }, 100);
    }
  }

  addEventListenerToMainPart(e) {
    return e;
    //    TODO: fix it
    //    this.routerGo = true;
    //    this._router.navigateByUrl('/buy/' + this.eventId + '/' + this.eventId + '/' + this.hallId + '/' + (<HTMLElement>e.target).getAttribute('section_name'));
  }

  onClick = e => {
    const target: any = e.target;
    if (
      target.classList.contains("seat") &&
      (target.getAttribute("ticketId") ||
        target.getAttribute("fanzone") === "true")
    ) {
      this.buyTicket(e);
    }
  }

  onMouseOver = e => {
    const target: any = e.target;

    if (target.classList.contains("seat")) {
      // this.showSeatTooltip(e);
    } else {
      // this.hideSeatTooltip(e);
    }
  }

  onMouseDown = e => {
    if (isPlatformBrowser(this.platformId)) {
      this.tempX = e.pageX;
      this.tempY = e.pageY;
      this.stop = true;
      this.hallMapEl.style.cursor = "-webkit-grabbing";
      document.addEventListener("mouseup", () => {
        this.hallMapEl.style.cursor = "";
        this.stop = false;
        document.removeEventListener("mousemove", this.onMouseMove);
      });
      document.addEventListener("mousemove", this.onMouseMove);
      e.preventDefault();
    }
  }

  onMouseMove = e => {
    if (window.innerWidth > 1024) {
      this.hallMapEl.style.left =
        this.hallMapEl.offsetLeft + e.pageX - this.tempX + "px";
      this.hallMapEl.style.top =
        this.hallMapEl.offsetTop + e.pageY - this.tempY + "px";
      this.tempX = e.pageX;
      this.tempY = e.pageY;
      e.preventDefault();
    }
  }

  onTouchDown = e => {
    if (isPlatformBrowser(this.platformId)) {
      this.tempX = e.touches[0].pageX;
      this.tempY = e.touches[0].pageY;
      this.stop = true;
      this.hallMapEl.style.cursor = "-webkit-grabbing";
      document.addEventListener("touchend", () => {
        this.hallMapEl.style.cursor = "";
        this.stop = false;
        document.removeEventListener("touchmove", this.onTouchMove);
      });
      document.addEventListener("touchmove", this.onTouchMove);
    }
  }

  onTouchMove = e => {
    if (window.innerWidth > 1024) {
      this.hallMapEl.style.left =
        this.hallMapEl.offsetLeft + e.touches[0].pageX - this.tempX + "px";
      this.hallMapEl.style.top =
        this.hallMapEl.offsetTop + e.touches[0].pageY - this.tempY + "px";
      this.tempX = e.touches[0].pageX;
      this.tempY = e.touches[0].pageY;
    }
  }

  onWheelPlus = e => {
    this.zoom("in");
  }

  onWheelMinus = e => {
    this.zoom("out");
  }

  onWheel = e => {
    if (e.deltaY < 0) {
      this.zoom("in");
    } else {
      this.zoom("out");
    }
  }

  ngAfterViewInit(): void {
    // const hammer = new Hammer(this.hallMapEl, {
    //   domEvents: true,
    // });
    // hammer.get('pinch').set({ enable: true });
    // hammer.on('pinch', (e) => {
    //   alert(e.toSource());
    //   this.pinchValue = e.scale;
    // });
    // hammer.on('pinchend', (e) => {
    //   alert(e.toSource());
    //   if (this.pinchValue > 0) {
    //     this.zoom('in');
    //   } else {
    //     this.zoom('out');
    //   }
    // });
  }

  zoom(zoomType) {
    if (!this.zoomIsActive) {
      this.hallMapEl.style.transition = "400ms all";
      this.zoomIsActive = true;
      const zoomStep = 1.56;
      if (zoomType === "in") {
        if (this.zooms <= 9) {
          this.zooms *= zoomStep;
        }
      } else {
        if (this.zooms >= 1) {
          this.zooms /= zoomStep;
        }
      }
    } else {
      return;
    }
    if (isPlatformBrowser(this.platformId)) {
      setTimeout(() => {
        this.zoomIsActive = false;
        this.hallMapEl.style.transition = "";
      }, 400);
    }
  }

  buyTicket(e: any) {
    this.isSelectedSeat.next(true);
    this.orderId = this.commonService.getOrderFromLocalStorage(this.eventId);
    const target: HTMLElement = e.target;
    const colorPrice = this.getColorPriceById(+target.getAttribute("colorPriceId"));
    if (isNullOrUndefined(this.orderId)) {
      this.bookingService.createBin(this.eventId).subscribe(
        orderId => {
          this.orderId = orderId;
          this.sendSelectedTicket(colorPrice, target);
        });
    } else {
      this.sendSelectedTicket(colorPrice, target);
    }
  }

  sendSelectedTicket(colorPrice, target) {
    if (isPlatformBrowser(this.platformId)) {
      if (target.getAttribute("fanzone") === "true") {
        if (this.commonService.getOrderFromLocalStorage(this.eventId)) {
          this.orderId = this.commonService.getOrderFromLocalStorage(this.eventId);
        } else {
          this.commonService.setOrderToLocalStorage(this.eventId, this.orderId, this.event.name, this.event.ticketOrderTime);
        }
        this.bookingTickets.idEvent = this.eventId;
        this.bookingTickets.idSeat = +target.getAttribute("seatId");
        this.bookingTickets.order = this.orderId;
        this.bookingTickets.typeOfScheme = TypeOfScheme.WITH_SCHEME;
        const tempTicket = this.allTickets.filter(value => value.seat.id === this.bookingTickets.idSeat);
        if (tempTicket.length > 0) {
          this.bookingTickets.comandBooking = CommandBooking.LOCK;
          // this.manageCountTicketsInBin();
          this.bookingTickets.idTicket =
            tempTicket[
              Math.floor(Math.random() * Math.floor(tempTicket.length))
              ].id;
          target.style.fill = SeatColorsEnum.Booked;
          if (isPlatformBrowser(this.platformId)) {
            setTimeout(() => {
              try {
                document.getElementById(target.getAttribute("id")).style.fill =
                  colorPrice.color;
              } catch (e) {
              }
            }, 500);
          }
          if (this.commonService.getOrderFromLocalStorage(this.eventId)) {
            this.orderId = this.commonService.getOrderFromLocalStorage(this.eventId);
            this.store.dispatch(new Booking(this.bookingTickets));
            this.bookingTickets = {};
          }
        } else {
          this.translateService.get("noFreePlacesFan").subscribe(text => {
            this.snackBarService.showSnackBar(text);
          });
          this.isSelectedSeat.next(false);
          // this.snackBarService.showSnackBar('Вже немає місць у фанзоні!');
          this.bookingTickets = {};
        }
      } else {
        // this.searchTicketById(+target.getAttribute("ticketId"), this.allTickets)) TODO it was here, but works incorrect
        if (target.style.fill !== SeatColorsEnum.Disabled && target.style.fill !== 'rgb(182, 182, 182)') {
          if (this.commonService.getOrderFromLocalStorage(this.eventId)) {
            this.orderId = this.commonService.getOrderFromLocalStorage(this.eventId);
          } else {
            this.commonService.setOrderToLocalStorage(this.eventId, this.orderId, this.event.name, this.event.ticketOrderTime);
          }
          this.orderId = this.commonService.getOrderFromLocalStorage(this.eventId);
          this.bookingTickets.idEvent = this.eventId;
          this.bookingTickets.idSeat = +target.getAttribute("seatId");
          this.bookingTickets.order = this.orderId;
          this.bookingTickets.typeOfScheme = TypeOfScheme.WITH_SCHEME;
          this.bookingTickets.idTicket = +target.getAttribute("ticketId");
          if (this.bin.indexOf(this.searchTicketById(+target.getAttribute("ticketId"), this.bin)) === -1) {
            this.bookingTickets.comandBooking = CommandBooking.LOCK;
            // this.manageCountTicketsInBin();
          } else {
            this.bookingTickets.comandBooking = CommandBooking.UN_LOCK;
            // this.manageCountTicketsInBin(false);
            if (this.bookingTickets.typeOfScheme === TypeOfScheme.WITH_SCHEME) {
              document.getElementById(target.getAttribute("id")).style.fill =
                colorPrice.color;
            }
          }
          this.store.dispatch(new Booking(this.bookingTickets));
          this.bookingTickets = {};
        } else {
          this.translateService.get("ticketIsLock").subscribe(text => {
            this.snackBarService.showSnackBar(text);
            this.isSelectedSeat.next(false);
          });
        }
      }
    }
  }

  // manageCountTicketsInBin(isInc = true) {
  //   const ordersInLocalStorage = JSON.parse(this.commonService.getFromLocalStorage('orders'));
  //   const currentOrder = ordersInLocalStorage.find(val => val.order + '' === this.orderId + '');
  //   if (!currentOrder.tickets) {
  //     currentOrder.tickets = 0;
  //   }
  //   if (isInc) {
  //     currentOrder.tickets = +currentOrder.tickets + 1;
  //   } else {
  //     currentOrder.tickets = +currentOrder.tickets - 1;
  //   }
  // }

  onRemoveTicket(id: number) {
    this.removedTicket.emit(id);
  }

  buyTicketWithOutSchema(id: number) {
    this.isSelectedSeat.next(true);
    const orderId: string = this.commonService.getOrderFromLocalStorage(this.eventId);
    const foundTicketDto = this.allTickets.find(ticketSchemeId => ticketSchemeId.ticketGroupWithOutSachem.id === id);
    let idTicket = null;
    if (foundTicketDto) {
      idTicket = foundTicketDto.id;
      this.allTickets = this.allTickets.filter(ticketId => ticketId.id !== idTicket);
    }

    const ticket: any = {
      idEvent: this.eventId,
      order: orderId,
      typeOfScheme: TypeOfScheme.WITHOUT_SCHEME,
      idTicket,
      comandBooking: CommandBooking.LOCK,
    };

    if (isNullOrUndefined(orderId)) {
      this.bookingService.createBin(this.eventId).subscribe(
        _orderId => {
          this.commonService.setOrderToLocalStorage(this.eventId, _orderId, this.event.name, this.event.ticketOrderTime);
          ticket.order = _orderId;
          this.store.dispatch(new Booking(ticket));
        });
    } else {
      this.store.dispatch(new Booking(ticket));
    }
  }

  setPriceToFilter(e, price, color) {
    if (isPlatformBrowser(this.platformId)) {
      e.currentTarget.classList.toggle("active");
      if (e.currentTarget.classList.contains("active")) {
        this.filterPriceState.push(price);
      } else {
        this.filterPriceState = this.filterPriceState.filter(val => val !== price);
      }
      for (let i = 0; i < this.allTickets.length; i++) {
        if (isPlatformBrowser(this.platformId)) {
          const seat = document.getElementById(this.allTickets[i].seat.key);
          if (this.filterPriceState.length) {
            if (
              this.filterPriceState.indexOf(+seat.getAttribute("price")) !== -1
            ) {
              seat.style.opacity = "1";
              seat.style.pointerEvents = "auto";
            } else {
              seat.style.opacity = "0.25";
              seat.style.pointerEvents = "none";
            }
          } else {
            seat.style.opacity = "1";
            seat.style.pointerEvents = "auto";
          }
        }
      }
    }
  }

  // Visual Filter
  getPriceFromVisualFilter() {
    // const filterPriceState = [];
    // const seats = document.querySelectorAll('*[price]');
    // const filterButtons = document.querySelectorAll('*[data-price]');
    // console.log('Hi there');
    // seats.forEach(seat => {
    //   seat.addEventListener('click', (e) => {
    //     e.preventDefault();
    //     if (seat.classList.contains('selected')) {
    //       seat.classList.remove('selected');
    //     } else if (!seat.classList.contains('selected') && !seat.hasAttribute('disabled')) {
    //       seat.classList.add('selected');
    //     }
    //   });
    // });
    // filterButtons.forEach(filterButton => {
    //   filterButton.addEventListener('click', (e) => {
    //     e.preventDefault();
    //     const currentPrice = parseFloat(filterButton.getAttribute('data-price'));
    //     console.log('CurrentPrice:', currentPrice);
    //     if (filterButton.classList.contains('selected')) {
    //       filterPriceState.splice(filterPriceState.indexOf(currentPrice), 1);
    //       filterButton.classList.remove('selected');
    //       console.log('remove class:', filterButton);
    //     } else {
    //       console.log(filterPriceState);
    //       filterPriceState.push(currentPrice);
    //       filterButton.classList.add('selected');
    //       console.log('add class:', filterButton);
    //     }
    //
    //     this.filterSeats(filterPriceState, seats);
    //   });
    // })
  }

  // filterSeats(filterPriceState, bool) {
  //   this.seats = document.querySelectorAll('path[price]');
  //   if (!bool) {
  //     console.warn('!bool');
  //     filterPriceState.map(price => {
  //       this.seats.forEach(seat => {
  //         seat.removeAttribute('disabled');
  //         seat.style.opacity = '1';
  //         seat.style.pointerEvents = 'auto';
  //         if (+seat.getAttribute('price') !== +price) {
  //           seat.setAttribute('disabled', 'true');
  //           seat.style.opacity = '0.25';
  //           seat.style.pointerEvents = 'none';
  //         }
  //       });
  //     });
  //   } else {
  //     if (filterPriceState.length === 1) {
  //       console.warn('=1');
  //       filterPriceState.map(price => {
  //         this.seats.forEach(seat => {
  //           seat.setAttribute('disabled', 'true');
  //           seat.style.opacity = '0.25';
  //           seat.style.pointerEvents = 'none';
  //           if (+seat.getAttribute('price') === +price) {
  //             seat.removeAttribute('disabled');
  //             seat.style.opacity = '1';
  //             seat.style.pointerEvents = 'auto';
  //           }
  //         });
  //       });
  //     } else if (filterPriceState.length > 1) {
  //       console.warn('>1');
  //       filterPriceState.map(price => {
  //         this.seats.forEach(seat => {
  //           if (+seat.getAttribute('price') === +price) {
  //             seat.removeAttribute('disabled');
  //             seat.style.opacity = '1';
  //             seat.style.pointerEvents = 'auto';
  //           }
  //         });
  //       });
  //     } else {
  //       console.warn('else');
  //       this.seats.forEach(seat => {
  //         seat.removeAttribute('disabled');
  //         seat.style.opacity = '1';
  //         seat.style.pointerEvents = 'auto';
  //       });
  //     }
  //   }
  // }

  // private onLoad() {
  //    window.onload = () => {
  //     setTimeout(this.getPriceFromVisualFilter, 100);
  //    };
  // }

  // private showSeatTooltip(e: MouseEvent) {
  //   const el: HTMLElement = e.target as any;
  //   this.seatInfo = {
  //     seatPosition: el.getAttribute('seat_position'),
  //     price: el.getAttribute('price'),
  //     fanzone: el.getAttribute('fanzone'),
  //     rowNumber: el.parentElement.getAttribute('row_number'),
  //     sectorName: el.parentElement.parentElement.getAttribute('sector_name'),
  //   };
  //   if (this.seatInfo.price) {
  //     const {clientWidth, clientHeight} = document.getElementById('tooltip-seat') as HTMLElement;
  //     const padding = 20;
  //     const top = e.pageY - (this.hallMapWrapperEl.offsetParent ? (this.hallMapWrapperEl.offsetParent as any).offsetTop : this.hallMapWrapperEl.offsetTop) - clientHeight - padding;
  //     const left = e.pageX - this.hallMapWrapperEl.offsetLeft - (clientWidth / 2);
  //     this.tooltipSeatStyles = {
  //       top: `${top}px`,
  //       left: `${left}px`,
  //       opacity: 1,
  //     };
  //   }
  // }
  //
  // private hideSeatTooltip(e) {
  //   this.tooltipSeatStyles = {opacity: 0};
  // }

  private paintSeats(tickets: TicketModel[]) {
    if (isPlatformBrowser(this.platformId)) {
      for (let i = 0; i < tickets.length; i++) {
        if (isPlatformBrowser(this.platformId)) {
          const seat = document.getElementById(tickets[i].seat.key);
          if (seat) {
            seat.style.fill = tickets[i].colorPrice.color;
            seat.setAttribute("seatId", tickets[i].seat.id + "");
            seat.setAttribute("price", tickets[i].colorPrice.price + "");
            seat.setAttribute("colorPriceId", tickets[i].colorPrice.id + "");
            if (tickets[i].seat.fanzone) {
              seat.setAttribute("fanzone", "true");
            } else {
              seat.setAttribute("ticketId", tickets[i].id + "");
              seat.setAttribute("fanzone", "false");
            }
          }
        }
      }
    }
  }

  private searchTicketById(id: number, tickets: Array<any>) {
    for (let i = 0; i < tickets.length; i++) {
      if (tickets[i].id === id) {
        return tickets[i];
      }
    }
    return null;

    // return tickets.find(ticket => ticket.id === id);
  }

  private getIndexOfArray(array: any[], id: number): number {
    for (let i = 0; i < array.length; i++) {
      if (array[i].id === id) {
        return i;
      }
    }
    return -1;
  }

  private getColorPriceById(id: number): ColorPriceModel {
    for (let i = 0; i < this.colorPrices.length; i++) {
      if (this.colorPrices[i].id === id) {
        return this.colorPrices[i];
      }
    }
    return null;
  }

  // private getColorPriceByPrice(price: number): ColorPriceModel {
  //   for (let i = 0; i < this.colorPrices.length; i++) {
  //     if (this.colorPrices[i].price === price) {
  //       return this.colorPrices[i];
  //     }
  //   }
  //   return null;
  // }
}
