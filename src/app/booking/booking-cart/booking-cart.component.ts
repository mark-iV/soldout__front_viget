import {
  Component,
  EventEmitter, HostListener,
  Input,
  OnChanges,
  OnInit,
  Output,
} from '@angular/core';
import {EventModel} from '@app/models/event.model';
import {BookedSeatModel} from '@app/models/booked-seat.model';
import {PriceModel} from '@app/models/price.model';
import {FormGroup} from '@angular/forms';
import {Subject} from 'rxjs';

declare var fbq: any;

@Component({
  selector: 'app-booking-cart',
  templateUrl: './booking-cart.component.html',
  styleUrls: ['./booking-cart.component.scss'],
})
export class BookingCartComponent implements OnInit, OnChanges {
  @Input() event: EventModel;
  @Input() bin: BookedSeatModel[];
  @Input() price: PriceModel;
  @Input() stepperRef;
  @Input() form: FormGroup;
  @Input() isSelectedSeat: Subject<boolean>;
  // accept: boolean;
  showTickets: boolean;
  paymentTickets: boolean;
  isMobileDevice: boolean;
  @Output() selectionChanged: EventEmitter<number> = new EventEmitter<number>();
  @Output() removedTicket: EventEmitter<BookedSeatModel> = new EventEmitter<BookedSeatModel>();

  constructor() {
  }

  ngOnInit() {
    // setInterval(() => {
    // console.log(this.bin);
    // }, 2000);
    this.showPaymentTickets(window.innerWidth);
    this.detectmob();
  }

  ngOnChanges(changes): void {
  }

  onSelectionChange(event) {
    this.selectionChanged.emit(event.value);
  }

  acceptTicket() {
    this.showTickets = true;
    this.paymentTickets = true;
  }

  removeTickets() {
    this.showTickets = false;
    this.paymentTickets = false;
  }

  nextStep() {
    this.stepperRef.next();
  }

  onRemoveTicket(id) {
    this.isSelectedSeat.next(true);
    if (this.bin.length === 1) {
      this.showPaymentTickets(window.innerWidth);
    }
    this.removedTicket.emit(this.bin.find(ticket => ticket.id === id));
  }

  showPaymentTickets(windowWidth) {
    if (windowWidth > 991) {
      this.showTickets = true;
      this.paymentTickets = true;
    } else {
      this.paymentTickets = false;
      this.showTickets = false;
    }
  }

  detectmob() {
    if (navigator.userAgent.match(/Android/i)
      || navigator.userAgent.match(/webOS/i)
      || navigator.userAgent.match(/iPhone/i)
      || navigator.userAgent.match(/iPad/i)
      || navigator.userAgent.match(/iPod/i)
      || navigator.userAgent.match(/BlackBerry/i)
      || navigator.userAgent.match(/Windows Phone/i)
    ) {
      this.isMobileDevice = true;
    } else {
      this.isMobileDevice = false;
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.showPaymentTickets(window.innerWidth);
  }

  @HostListener('window:onload', ['$event'])
  onLoad() {
    this.showPaymentTickets(window.innerWidth);
  }

  smmInitCheckout() {
    try {
      fbq('track', 'InitiateCheckout');
    } catch (e) {
    }
  }
}
