import {
  Component,
  Input,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChildren,
  ChangeDetectorRef,
  Renderer2,
  PLATFORM_ID,
  Inject,
} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {select, Store} from '@ngrx/store';
import {IAppState} from '@app/+store';
import {getBin, getPrice} from '@app/+store/booking/booking.selector';
import {Observable, Subscription, Subject} from 'rxjs';
import {BookedSeatModel} from '@app/models/booked-seat.model';
import {ActivatedRoute, Params, Router, NavigationEnd} from '@angular/router';
import {EventModel} from '@app/models/event.model';
import {getEvent} from '@app/+store/event/event.selector';
import {GetEvent} from '@app/+store/event/event.actions';
import {LanguageIds, SeatColorsEnum} from '@app/app.settings';
import {Booking, GetPrice, GetBin} from '@app/+store/booking/booking.actions';
import {CommandBooking} from '@app/enums/command-booking.enum';
import {BookingModel} from '@app/models/booking.model';
import {TypeOfScheme} from '@app/enums/type-of-scheme.enum';
import {CommonService} from '@app/services';
import {MatDialog, MatHorizontalStepper, MatSidenav} from '@angular/material';
import {TimerTimeoutModalComponent} from '@app/shared/components/modals/timer-timeout-modal/timer-timeout-modal.component';
import {LanguageModel} from '@app/models/language.model';
import {getLanguageState} from '@app/+store/language/language.selectors';
import {BookingService} from '@app/services/booking.service';
import {tap, take, map, finalize} from 'rxjs/operators';
import {BookedOrderModel} from '@app/models/booked-order.model';
import {ExitModalComponent} from '@app/shared/components/modals/exit-modal/exit-modal.component';
import {SnackBarService} from '@app/services/snack-bar.service';
import {isPlatformBrowser} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {Title} from "@angular/platform-browser";
import {LoaderService} from "@app/services/loader.service";

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss'],
})
export class BookingComponent implements OnInit, OnDestroy {
  @ViewChildren(MatHorizontalStepper) stepperRef: QueryList<MatHorizontalStepper>;
  bin: BookedSeatModel[] = [];
  event$: Observable<EventModel>;
  price$: Observable<any>;
  promoCodeValue = undefined;
  price: any;
  @Input() event: EventModel = {} as EventModel;
  @Input() events: EventModel[] = [];
  @Input() inputSideNav: MatSidenav;
  eventId: number;
  timerTo: string;
  editableDisable: Boolean;
  currentLang: LanguageModel;
  iframeSrc: string;

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;

  newEventId: number;

  // editable: boolean = false;

  s: Subscription[] = [];
  public showTimer = false;

  activeSell = false;

  isPaymentStep = false;

  orderNumber: string;

  colorPrices: any[];
  bonusesPrice = 0;
  currentUrl: any;
  siteUrl: string;

  isSelectedSeat = new Subject<boolean>();
  lang$: Observable<any>;
  lang;


  constructor(private fb: FormBuilder,
              private store: Store<IAppState>,
              private route: ActivatedRoute,
              private router: Router,
              private commonService: CommonService,
              private dialog: MatDialog,
              private bookingService: BookingService,
              private _loadingService: LoaderService,
              private changeDetectorRef: ChangeDetectorRef,
              private renderer: Renderer2,
              private snackBarService: SnackBarService,
              private translateService: TranslateService,
              @Inject(PLATFORM_ID) private platformId: Object,
              private titleService: Title) {
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.currentUrl = event.url;
        this.renderer.removeClass(document.body, 'body-booking-overflow');
        if (this.currentUrl.includes('booking')) {
          this.renderer.addClass(document.body, 'body-booking-overflow');
        }
      }
    });
  }

  ngOnInit() {
    this.siteUrl = document.referrer;
    console.log(this.siteUrl);
    if (isPlatformBrowser(this.platformId)) {
      this.lang$ = this.store.pipe(select(getLanguageState), map(v => v.id)), tap(v => {
        this.lang = v;
      });
      this._loadingService.modifyLoader(true);
      this.editableDisable = true;
      this.firstFormGroup = this.fb.group({
        bin: this.fb.array([], Validators.required),
      });
      this.secondFormGroup = this.fb.group({
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        mail: ['', Validators.required],
        phone: ['', Validators.required],
        promoCode: [''],
        useBonuse: [''],
      });
      this.thirdFormGroup = this.fb.group({
        card: ['', Validators.required],
        month: ['', Validators.required],
        year: ['', Validators.required],
        cvv: ['', Validators.required],
        cardholder: ['', Validators.required],
      });
      this.store.pipe(select(getBin)).subscribe(bin => {
        setTimeout(() => {
          this.isSelectedSeat.next(false);
        }, 300);
        if (bin) {
          this.bin = bin;
          this.changeStatusTimerByBin(bin);
          bin.forEach(ticket => {
            if (ticket.seat) {
              const el = document.getElementById(ticket.seat.key);
              if (el) {
                if (el.getAttribute('fanzone') === 'false') {
                  el.style.fill = SeatColorsEnum.Booked;
                }
              }
            }
          });
          this.bonusesPrice = this.countBonusesPrice();
          this.firstFormGroup.setControl('bin', this.fb.array(bin, Validators.required));
        } else {
          this.bin = [];
          this.commonService.removeOrderFromLocalStorage(this.eventId);
        }
      });
      // if (isPlatformBrowser(this.platformId)) {
      this.event$ = this.store.pipe(select(getEvent), tap(val => {
        if (val && val.id === this.newEventId) {
          if (new Date().getTime() < new Date(val.beginSell).getTime() || !val.beginSell
            || new Date().getTime() > new Date(val.beginEndDate).getTime()) {
            this.translateService.get('salesAreStoped').subscribe(text => {
              this.snackBarService.showSnackBar(text);
            });
            // this.snackBarService.showSnackBar('Продажі призупинено!');
            this.router.navigate(['/home'], {queryParams: {lang: this.lang}}).then();
            // this._loadingService.modifyLoader(false);
          } else {
            this.activeSell = true;
            this.store.pipe(select(getLanguageState), take(1), tap((lang) => {
              if (lang.id.toLowerCase() === 'ua') {
                this.titleService
                  .setTitle(`Квиток на ${val.name.valueUA} в місті ${val.platformHall.city.dictionary.valueUA} ${new Date(val.dataBegin).toLocaleString('ua-UK')} | Soldout`);
              } else if (lang.id.toLowerCase() === 'ru') {
                this.titleService
                  .setTitle(`Билет на ${val.name.valueRU} в городе ${val.platformHall.city.dictionary.valueRU} ${new Date(val.dataBegin).toLocaleString('ua-UK')} | Soldout`);
              } else {
                this.titleService
                  .setTitle(`Квиток на ${val.name.valueUA} в місті ${val.platformHall.city.dictionary.valueUA} ${new Date(val.dataBegin).toLocaleString('ua-UK')} | Soldout`);
              }
            })).subscribe();
            // this._loadingService.modifyLoader(false);
          }
        }
      }));
      // }
      this.price$ = this.store.pipe(select(getPrice), tap(val => {
        this.price = val;
        if (this.promoCodeValue) {
          this.onPromoCode(this.promoCodeValue);
        }
      }));


      this.s.push(this.store.pipe(select(getLanguageState)).subscribe((language: LanguageModel) => this.currentLang = language));


      this.secondFormGroup.reset();

      this.route.params.pipe(take(1)).subscribe((param: Params) => {
        this.eventId = +(param.eventId.split('-')[0]);
        if (!this.newEventId) {
          this.newEventId = this.eventId;
        }
        this.store.dispatch(new GetEvent(this.eventId));
        // this.store.dispatch(new GetBinSuccess([]));
        // this.store.dispatch(new GetPriceSuccess(null));
        this.orderNumber = this.commonService.getOrderFromLocalStorage(this.eventId);
        if (this.orderNumber) {
          this.store.dispatch(new GetBin(this.orderNumber));
          this.store.dispatch(new GetPrice(this.orderNumber));
        } else {
          this.store.dispatch(new GetPrice(null));
          this.store.dispatch(new GetBin(null));
        }
      });
    }
  }

  ngOnDestroy(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.commonService.unsubscribeAll(this.s);
    }
  }

  canDeactivate(): boolean | Observable<boolean> {
    if (this.isPaymentStep) {
      const exitDialog = this.dialog.open(ExitModalComponent, {
        width: '600px',
        data: 'booking',
      });
      return exitDialog.afterClosed().pipe(map(val => val ? true : false), tap(val => {
        if (val) {
          this.bookingService.destroy(+this.orderNumber).subscribe();
        }
      }));
    } else {
      return true;
    }
  }

  onSelectionChange(eventId: number) {
    // if (isPlatformBrowser(this.platformId)) {
    this.store.dispatch(new GetEvent(eventId));
    // this._loadingService.modifyLoader(true);
    this.event$ = undefined;
    this.price$ = undefined;
    this.promoCodeValue = undefined;
    this.price = undefined;
    this.eventId = undefined;
    this.timerTo = undefined;
    this.editableDisable = undefined;
    this.currentLang = undefined;
    this.iframeSrc = undefined;
    this.s = [];
    this.showTimer = false;
    this.activeSell = false;
    this.changeDetectorRef.detectChanges();
    this.newEventId = eventId;
    this.orderNumber = undefined;
    this.router.navigate(['/booking', eventId], {queryParams: {lang: this.lang}}).then(() => {
      setTimeout(() => {
        this.ngOnInit();
      }, 1000);
    });
    // }
  }

  onTimerFinished() {
    const waitFor = (ms) => new Promise(r => setTimeout(r, ms));

    async function asyncForEach(array, callback) {
      for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
      }
    }

    const ordersFromLS = this.commonService.getAllOrdersFromLS();
    asyncForEach(ordersFromLS, async (order) => {
      await waitFor(100);
      asyncForEach(this.bin, async (item) => {
        await waitFor(330);
        // this.onRemoveTicket(item, order);
      }).then();
    }).then(() => {
      const o = this.commonService.getOrderFromLocalStorage(this.eventId);
      this.commonService.removeOrderFromLocalStorage(this.eventId);
      const timerDialog = this.dialog.open(TimerTimeoutModalComponent, {
        width: '600px',
        panelClass: 'timer-dialog-custom',
      });
      timerDialog.afterClosed().subscribe(val => {
        this.bookingService.destroy(o).pipe(finalize(() => {
          if (!val) {
            // this._loadingService.modifyLoader(true);
            this.onSelectionChange(this.eventId);
          }
        })).subscribe();
        if (val) {
          this.router.navigate(['/home'], {queryParams: {lang: this.lang}}).then();
        }
      });
    });
  }

  onStepChange(event: any) {
    if (event.selectedIndex === 2) {
      this.editableDisable = false;
      this.isPaymentStep = true;
    } else if (event.selectedIndex < 2) {
      this.editableDisable = true;
    }
    const values = this.secondFormGroup.getRawValue();
    this.secondFormGroup.patchValue(values);
  }

  onRemoveTicket(bookedSeat: BookedSeatModel, order: BookedOrderModel) {
    // if (isPlatformBrowser(this.platformId)) {
    const typeOfScheme = bookedSeat.ticketGroupWithOutSachem ? TypeOfScheme.WITHOUT_SCHEME : TypeOfScheme.WITH_SCHEME;
    const seat: BookingModel = {
      idTicket: bookedSeat.id,
      idEvent: this.eventId,
      order: order ? order.order : this.commonService.getOrderFromLocalStorage(this.eventId),
      comandBooking: CommandBooking.UN_LOCK,
      typeOfScheme,
    };

    if (typeOfScheme === TypeOfScheme.WITH_SCHEME) {
      seat.idSeat = bookedSeat.seat.id;
      document.getElementById(bookedSeat.seat.key).style.fill = bookedSeat.colorPrice.color;
    }
    this.store.dispatch(new Booking(seat));
    // }
    // const o = order ? order.order : this.commonService.getOrderFromLocalStorage(this.eventId);
    // this.manageCountTicketsInBin(o);
  }

  // manageCountTicketsInBin(order: string) {
  //   const ordersInLocalStorage = JSON.parse(this.commonService.getFromLocalStorage('orders'));
  //   const currentOrder = ordersInLocalStorage.find(val => val.order + '' === order + '');
  //   if (!currentOrder.tickets) {
  //     return;
  //   }
  //   currentOrder.tickets = +currentOrder.tickets - 1;
  //   this.commonService.setToLocalStorage('orders', JSON.stringify(ordersInLocalStorage), true);
  // }

  onBuy() {

    const language = this.getLanguage();
    this.orderNumber = this.commonService.getOrderFromLocalStorage(this.eventId);
    const bookingConfig: any = {
      user: this.getUserData(),
      order: this.orderNumber,
      comment: '',
    };
    if (this.promoCodeValue) {
      bookingConfig.promoCode = this.secondFormGroup.getRawValue()['promoCode'];
    } else if (Boolean(this.secondFormGroup.getRawValue()['useBonuse'])) {
      bookingConfig.useBonuse = this.secondFormGroup.getRawValue()['useBonuse'];
    }
    this.bookingService.orderBuy(bookingConfig, language, this.siteUrl)
      .subscribe(res => {
        if(res.flag_get_url) {
          this.iframeSrc = res.url;
        } else {
          window.top.location.href = res.url;
        }
        this.stepperRef.first.next();
        this.commonService.removeOrderFromLocalStorage(this.eventId);
      }, err => {
        this.translateService.get('errorDefault').subscribe(text => {
          this.snackBarService.showSnackBar(text);
        });
        this.onSelectionChange(this.eventId);
      });
  }

  changeStatusTimerByBin(bin: BookedSeatModel[]) {
    if (bin.length && !this.showTimer) {
      return this.startTimer();
    } else if (!bin.length) {
      return this.removeTimer();
    }

  }

  startTimer() {
    this.showTimer = true;
  }

  removeTimer() {
    this.showTimer = false;
  }

  onPromoCode(code) {
    this.promoCodeValue = code;
  }

  private getUserData(): { firstName: string, lastName: string, mail: string, phone: string } {
    const {firstName, lastName, mail, phone} = this.secondFormGroup.getRawValue();
    return {
      firstName,
      lastName,
      mail,
      phone,
    };
  }

  private getLanguage(): 'RU' | 'EN' | 'UK' {
    switch (this.currentLang.id) {
      case LanguageIds.En:
        return 'EN';
      case LanguageIds.Ru:
        return 'RU';
      case LanguageIds.Ua:
      default:
        return 'UK';
    }
  }

  onGetColorPrices(cP) {
    this.colorPrices = cP;
  }

  countBonusesPrice(): number {
    // if (isPlatformBrowser(this.platformId)) {
    if (this.colorPrices) {
      // This is a mistake, if I use reduce. With forEach everything is all right. WHY?
      // return this.bin.reduce((sum, item) => {
      //   const tempCP = this.colorPrices.find(v => v.id === item.colorPrice.id);
      //   console.log(sum + tempCP ? tempCP.bonusSell : 0);
      //   return sum + tempCP ? tempCP.bonusSell : 0;
      // }, 0);
      let sum = 0;
      this.bin.forEach(item => {
        const tempCP = this.colorPrices.find(v => v.id === item.colorPrice.id);
        sum += tempCP ? tempCP.bonusSell : 0;
      });
      return sum;
    } else {
      setTimeout(() => {
        this.countBonusesPrice();
      }, 2000);
    }
    // }
  }
}
