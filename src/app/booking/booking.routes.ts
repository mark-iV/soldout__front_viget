import { Routes } from '@angular/router';
import { BookingComponent } from '@app/booking/booking.component';


 export const bookingRoutes: Routes = [
  {
    path: ':eventId',
    component: BookingComponent,
  },
];

