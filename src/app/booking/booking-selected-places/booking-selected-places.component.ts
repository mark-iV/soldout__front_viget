import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { EventModel } from '@app/models/event.model';
import { BookedSeatModel } from '@app/models/booked-seat.model';

@Component({
  selector: 'app-booking-selected-places',
  templateUrl: './booking-selected-places.component.html',
  styleUrls: ['./booking-selected-places.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BookingSelectedPlacesComponent implements OnInit {
  @Input() event: EventModel;
  @Input() bin: BookedSeatModel;

  constructor() { }

  ngOnInit() {
  }

}
