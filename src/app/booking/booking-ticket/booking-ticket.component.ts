import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-booking-ticket',
  templateUrl: './booking-ticket.component.html',
  styleUrls: ['./booking-ticket.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BookingTicketComponent implements OnInit {
  @Input() id: number;
  @Input() row: string;
  @Input() seat: any;
  @Input() seatPosition: string;
  @Input() sector: string;
  @Input() color: string;
  @Input() price: string;
  @Input() name: string;
  @Input() comment: string;
  @Input() canRemove = true;

  @Output() removed = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {

  }

  remove() {
    this.removed.emit(this.id);
  }

}
