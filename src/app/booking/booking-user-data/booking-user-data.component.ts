import { Component, EventEmitter, Input, OnInit, Output, PLATFORM_ID, Inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { PHONE_MASK } from '@app/app.settings';
import { BookingService } from '@app/services/booking.service';
import { SnackBarService } from '@app/services/snack-bar.service';
import { LoaderService } from '@app/services/loader.service';
import { isPlatformBrowser } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from '@app/services';

declare var fbq: any;

@Component({
  selector: 'app-booking-user-data',
  templateUrl: './booking-user-data.component.html',
  styleUrls: ['./booking-user-data.component.scss'],
})
export class BookingUserDataComponent implements OnInit {
  @Input() isReadOnly = false;
  @Input() price;
  @Input() bin;
  @Input() form: FormGroup;
  @Input() eventId;
  @Input() lang = 'ua';
  @Input() eventData: any;
  showPromoCode = false;
  showBonus = false;
  submittedForm = false;
  acceptOfferta = false;
  phoneMask = PHONE_MASK;
  @Output() buy: EventEmitter<void> = new EventEmitter<void>();
  @Output() promoCode: EventEmitter<any> = new EventEmitter<any>();

  promoCodePrice = undefined;

  isShowText = false;

  constructor(private bookingService: BookingService,
              private snackBarService: SnackBarService,
              private _loadingService: LoaderService, @Inject(PLATFORM_ID) private platformId: Object,
              private translateService: TranslateService, private _commonService: CommonService) {
  }

  togglePromoCode(code: HTMLInputElement) {
    if (!(<HTMLInputElement>event.target).checked) {
      this.removePromoCode(code);
    }
    this.showBonus = false;
    this.form.controls['useBonuse'].setValue(false);
  }

  nextStep() {
    if (isPlatformBrowser(this.platformId)) {
      this.submittedForm = true;
      if (this.form.valid) {
        try {
          fbq('track', 'CompleteRegistration');
        } catch (e) {
        }
        this.buy.emit();
        setTimeout(() => {
          this.submittedForm = false;
        }, 10000);
      }
    }
  }

  sendPromoCode(code: HTMLInputElement) {
    const order = this._commonService.getOrderFromLocalStorage(this.eventId);
    if (code.value.trim()) {
      this.bookingService.checkPromoCode(code.value, this.eventId, order).subscribe(next => {
        this.promoCode.emit(next);
        code.disabled = true;
        this.promoCodePrice = next;
        // promoContainer.classList.add('hide');
      }, error => {
        this.translateService.get('invalidPromoCode').subscribe(text => {
          this.snackBarService.showSnackBar(text);
        });
        this.removePromoCode(code);
      });
    }
  }

  removePromoCode(code: HTMLInputElement) {
    try {
      code.disabled = false;
      code.value = '';
      this.showPromoCode = false;
      this.promoCodePrice = undefined;
      this.promoCode.emit(undefined);
    } catch (e) {
    }

  }

  ngOnInit(): void {
    this._loadingService.modifyLoader(false);
    this.form.valueChanges.subscribe(next => {
      this.submittedForm = false;
      if (next.useBonuse) {
        this.showPromoCode = false;
        this.showBonus = true;
      } else {
        this.showBonus = false;
      }
    });
  }

  isUsedBonuses() {
    return this.form.controls['useBonuse'].value;
  }

  countCommision(price, commision) {
    return +((price * commision) / 100).toFixed(2);
  }
}
