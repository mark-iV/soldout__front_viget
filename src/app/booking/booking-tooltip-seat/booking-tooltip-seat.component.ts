import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-booking-tooltip-seat',
  templateUrl: './booking-tooltip-seat.component.html',
  styleUrls: ['./booking-tooltip-seat.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BookingTooltipSeatComponent {
  @Input() data;
}
