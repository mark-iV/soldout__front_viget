import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/shared/shared.module';
import { MatButtonToggleModule, MatStepperModule } from '@angular/material';
import { bookingRoutes } from '@app/booking/booking.routes';
import { BookingComponent } from '@app/booking/booking.component';
import { BookingHallMapComponent } from './booking-hall-map/booking-hall-map.component';
import { BookingCartComponent } from '@app/booking/booking-cart/booking-cart.component';
import { BookingTicketComponent } from './booking-ticket/booking-ticket.component';
import { BookingSelectedPlacesComponent } from './booking-selected-places/booking-selected-places.component';
import { BookingUserDataComponent } from './booking-user-data/booking-user-data.component';
import { BookingTooltipSeatComponent } from './booking-tooltip-seat/booking-tooltip-seat.component';
import { PinchZoomModule } from 'ngx-pinch-zoom';

@NgModule({
  declarations: [
    BookingComponent,
    BookingHallMapComponent,
    BookingCartComponent,
    BookingTicketComponent,
    BookingSelectedPlacesComponent,
    BookingUserDataComponent,
    BookingTooltipSeatComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(bookingRoutes),
    SharedModule,
    MatStepperModule,
    MatButtonToggleModule,
    PinchZoomModule,
  ],
})
export class BookingModule { }
