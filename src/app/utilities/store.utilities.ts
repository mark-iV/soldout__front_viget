import { union } from '@ngrx/store';

export const getActionsByClass = (cls) => {
  const actions = Object.getOwnPropertyNames(cls)
    .filter(prop => typeof cls[prop] === 'function')
    .reduce((acc, prop) => acc[prop] = cls[prop], {});

  return union(actions);
};

