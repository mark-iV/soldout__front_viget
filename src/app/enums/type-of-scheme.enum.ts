export enum TypeOfScheme {
  WITH_SCHEME = 'WITH_SCHEME',
  WITHOUT_SCHEME = 'WITHOUT_SCHEME',
}
