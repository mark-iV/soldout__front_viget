export class EnvironmentInterface {
  production: boolean;
  environmentName: string;
  facebookClientId: string;
  googleClientId: string;
}
